### A Pluto.jl notebook ###
# v0.12.6

using Markdown
using InteractiveUtils

# ╔═╡ eab07330-0e48-11eb-326b-3d189cd7bf3c
md"""
# Процедуры как вычислительные объекты

В математике часто можно встретить операции, производимые над функциями.

Например:
1) поиск экстремумов $\max\limits_{x \in S} f(x)$
2) суммирование $\sum_{i=1}^n f(x_i)$
3) вычисление производной $g(x) = df(x) / dx$
4) вычисление первообразной $g(x) = \int\limits_0^x f(\xi) d\xi$

В первых двух случаях функции ставится в соответствие некоторое число, во вторых двух - функции ставится в соответствие другая функция. Можно также придумать пример операции, ставящей числу в соответствие функцию - например, операцию частичной суммы ряда Маклорена для синуса можно рассматривать как функцию, принимающую целое число $n$ и возвращающую функцию - частичную сумму до степени $n$.

Во многих современных языках программирования, включая Julia, имеется возможность работать с процедурами (являющимися "аналогами" математических функций) как с операндами и возвращаемыми значениями других процедур. Процедуры, которые могут принимать другие процедуры аргументами или возвращают их как значения, называются *процедурами высшего порядка* (higher-order procedures).

Рассмотрим для знакомства пример суммирования последовательностей.
"""

# ╔═╡ 4fb26c36-0e56-11eb-2dfc-0998c0bad2fb
function sum_integers(a, b)
	if a > b
		return 0
	else
		return a + sum_integers(a+1, b)
	end
end

# ╔═╡ 73b46dfa-0e56-11eb-1258-e7d852c542bb
md"""
Процедура `sum_integers` вычисляет сумму последовательных натуральных чисел $a + (a+1) + \dots + b$.

Чуть сложнее можно записать другую сумму:
```math
1 - \frac{1}{3} + \frac{1}{5} - \frac{1}{7} + \cdots = \frac{2}{1 \cdot 3} + \frac{2}{5 \cdot 7} + \frac{2}{9 \cdot 11} + \dots
```
Эта сумма представляет собой ряд Маклорена для арктангенса $\arctan(x) = \sum\limits_{i=0}^{\infty}(-1)^i\cfrac{x^{2i+1}}{2i+1}$ при $x=1$ и, таким образом, сходится к $\arctan(1) = \pi / 4$.
"""

# ╔═╡ 31f74094-0e57-11eb-1f1b-65392a2d1be7
function pi_sum_1(a, b)
	if a > b
		return 0.0
	else
		return 2 / (a * (a + 2)) + pi_sum_1(a + 4, b)
	end
end

# ╔═╡ 4ce02910-0e58-11eb-26cd-b9d1083da514
md"""
Процедуры `sum_integers` и `pi_sum` можно записать в виде одного и того же шаблона:
```julia
function fn(a, b)
	if a > b
		return ⟨init_value⟩
	else
		return ⟨term⟩(a) + fn(⟨next⟩(a), b)
	end
end
```
Эту общую структуру было бы неплохо использовать, чтобы выразить обе процедуры в единообразной форме. В Julia мы можем определить процедуру, которая в дополнение к `a` и `b` будет принимать процедуры `term` и `next` и начальное значение `init_value`:

```julia
function sum_sequence(term, a, next, b, init_value)
	if a > b
		return init_value
	else
		return term(a) + sum_sequence(term, next(a), next, b, init_value)
	end
end
```

Когда у процедуры так много аргументов, в них несложно запутаться. В частности, можно случайно передать аргументы не в том порядке. Для этого случая в Julia есть синтаксис *именованных аргументов*. В списке аргументов процедуры именованные аргументы идут после позиционных и отделяются точкой с запятой:
"""

# ╔═╡ 415e4896-0e59-11eb-3851-3f1e75efe118
function sum_sequence(a, b; term, next, init_value)
	if a > b
		return init_value
	else
		return term(a) + sum_sequence(next(a),
			                          b,
			                          term=term,
			                          next=next,
			                          init_value=init_value)
	end
end

# ╔═╡ b4f5ab90-0e5a-11eb-0ce3-bd07b2c06e23
md"""
Как видно из записи рекуррентного вызова, именованные аргументы подставляются в вызов в форме `⟨имя аргумента⟩=⟨значение⟩`.

С этой обобщённой процедурой предыдущие можно записать проще:
"""

# ╔═╡ 0feb6332-0e5b-11eb-0c58-792b5a75ff93
function sum_ints(a, b)
	inc(n) = n + 1
	return sum_sequence(a, b, term=identity, next=inc, init_value=0)
end

# ╔═╡ 3b836ca6-0e5b-11eb-05ed-5587fd57b9f0
sum_ints(1, 10) == sum_integers(1, 10)

# ╔═╡ 4d9f9892-0e5b-11eb-11ba-ef4bceef23e2
function pi_sum_2(a, b) 
	pi_next(n) = n + 4
	pi_term(n) = 2 / (n * (n + 2))
	return sum_sequence(a, b, term=pi_term, next=pi_next, init_value=0.0)
end

# ╔═╡ ddbc3868-0e5b-11eb-32fc-779c8eed50c7
pi_sum_2(1, 1000) == pi_sum_1(1, 1000)

# ╔═╡ ff5544bc-0e5c-11eb-31ef-d16c01881602
md"""
**Упражнение:** Описанная процедура `sum_sequence` порождает линейную рекурсию. Можно переписать её так, чтобы она порождала хвостовую рекурсию:
```julia
function sum_sequence(a, b; term, next, init_value)
	function tailrec(a, result)
		if ⟨???⟩
			return ⟨???⟩
		else
		return tailrec(⟨???⟩, ⟨???⟩)
	end
	tailrec(⟨???⟩, ⟨???⟩)
end
```
Заполните пропущенные выражения.
"""

# ╔═╡ 851c0542-0e5d-11eb-2416-bd194fc1bdbe


# ╔═╡ 887609e0-0e5d-11eb-041b-0bb41bd3f5ff
md"""
## Анонимные процедуры

При использовании процедур высшего порядка иногда нужно передать в них довольно тривиальные процедуры как аргументы. Необходимость создавать их и давать им имя кажется излишней, хотелось бы способа, например, вместо `pi_next` записать выражение вроде "процедура, возвращающая свой аргумент плюс 2".

В Julia такая запись делается "стрелочным выражением", имеющим форму
```julia
⟨список аргументов⟩ -> ⟨тело процедуры⟩
```
Такие выражения называют также "анонимные процедуры" или "лямбда-выражения".

С анонимными процедурами `pi_sum` запишется какбез доопределения вспомогательных процедур
"""

# ╔═╡ 49ee1fe0-0e5e-11eb-031f-77e1a6b8c3ab
pi_sum_3(a, b) = sum_sequence(a,
	                          b,
	                          term = n -> 2 / (n * (n + 2)),
	                          next = n -> n + 4,
	                          init_value = 0.0)

# ╔═╡ 869c4a52-0e5e-11eb-0307-69c8a70339c8
pi_sum_3(1, 1000) == pi_sum_1(1, 1000)

# ╔═╡ b9f9bd7e-0e60-11eb-1da2-6937be28a1f9
md"""
## Процедуры как возвращаемые значения

Поскольку в некоторых случаях удобно передавать процедуру как аргумент, было бы также удобно создавать "генераторы процедур", которые бы возвращали процедуры как значения. В синтаксисе Julia это делается тривиально.

Например, процедура `adder(x)` возвращает процедуру, прибавляющую `x` к своему аргументу:
"""

# ╔═╡ 18c9378e-0e62-11eb-3bd5-95a1542b1920
adder(x) = y -> x + y

# ╔═╡ 2d6b8002-0e62-11eb-2284-1fa3c7b29042
begin
	add5 = adder(5)
	add5(37)
end

# ╔═╡ 51f2a176-0e62-11eb-326a-5b70779419e1
md"""
Менее тривиальный пример - получение численного приближения производной:
"""

# ╔═╡ 6fc177a6-0e62-11eb-14af-cdc92b472fc5
function numerical_deriv(fn, dx)
	return x -> (fn(x + dx) - fn(x)) / dx
end

# ╔═╡ 91b08120-0e62-11eb-368e-05cb36d051cf
(numerical_deriv(x -> x^2, 1e-8))(1.0)

# ╔═╡ fd04664e-0e62-11eb-0815-4d85bcaed741
md"""
Эти примеры покаывают, что процедуры в Julia являются *объектами первого класса*, то есть:
* их можно присваивать переменным
* их можно передавать как аргументы в процедуры
* их можно возвращать из процедур
* они могут быть элементами структур данных

Эти свойства дают широкие возможности для написания более читаемых и более выразительных программ. Интерпретатор языка Julia написан так, чтобы использование процедур как объектов в как можно меньшей мере приводило к накладным расходам, так что программы с использованием процедур высшего порядка нередко не имеют каких-либо "штрафов" по производительности по сравнению с ручным расписыванием частного случая для обобщённого алгоритма.
"""

# ╔═╡ Cell order:
# ╟─eab07330-0e48-11eb-326b-3d189cd7bf3c
# ╠═4fb26c36-0e56-11eb-2dfc-0998c0bad2fb
# ╟─73b46dfa-0e56-11eb-1258-e7d852c542bb
# ╠═31f74094-0e57-11eb-1f1b-65392a2d1be7
# ╟─4ce02910-0e58-11eb-26cd-b9d1083da514
# ╠═415e4896-0e59-11eb-3851-3f1e75efe118
# ╟─b4f5ab90-0e5a-11eb-0ce3-bd07b2c06e23
# ╠═0feb6332-0e5b-11eb-0c58-792b5a75ff93
# ╠═3b836ca6-0e5b-11eb-05ed-5587fd57b9f0
# ╠═4d9f9892-0e5b-11eb-11ba-ef4bceef23e2
# ╠═ddbc3868-0e5b-11eb-32fc-779c8eed50c7
# ╟─ff5544bc-0e5c-11eb-31ef-d16c01881602
# ╠═851c0542-0e5d-11eb-2416-bd194fc1bdbe
# ╟─887609e0-0e5d-11eb-041b-0bb41bd3f5ff
# ╠═49ee1fe0-0e5e-11eb-031f-77e1a6b8c3ab
# ╠═869c4a52-0e5e-11eb-0307-69c8a70339c8
# ╟─b9f9bd7e-0e60-11eb-1da2-6937be28a1f9
# ╠═18c9378e-0e62-11eb-3bd5-95a1542b1920
# ╠═2d6b8002-0e62-11eb-2284-1fa3c7b29042
# ╟─51f2a176-0e62-11eb-326a-5b70779419e1
# ╠═6fc177a6-0e62-11eb-14af-cdc92b472fc5
# ╠═91b08120-0e62-11eb-368e-05cb36d051cf
# ╟─fd04664e-0e62-11eb-0815-4d85bcaed741
