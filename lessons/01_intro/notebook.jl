### A Pluto.jl notebook ###
# v0.11.14

using Markdown
using InteractiveUtils

# ╔═╡ c328d2a4-efa0-11ea-25d1-8b0ec2850200
using BenchmarkTools, Libdl

# ╔═╡ f3123a28-efaf-11ea-2064-4d1e5dfa338c
md"""
# Зачем нужен язык Julia?

Julia - динамический язык программирования, разрабатываемый для "технических" приложений: реализации алгоритмов для решения численных задач, обработки данных, численного моделирования и т.п. По выражению команды разработчиков, язык Julia "ходит как Python, бегает как Си" ("walks like Python, runs like C"). Таким образом подчёркивается, что написание программ обычно идёт на довольно высоком уровне (как на Python), но правильно написанная программа работает быстро (сопоставимо с программой, написанной на Си).

Посмотрим, как это выглядит на примере программы для вычисления стандартного отклонения по массиву чисел:

```math
\begin{split}
\sigma_n^2 &= \sqrt{\frac{1}{n-1}{\sum\limits_{i=1}^n{(x_i - \bar{x})^2}}} \\
\bar{x} &= \frac{1}{n}{\sum\limits_{i=1}^{n}{x_i}}
\end{split}
```
"""

# ╔═╡ 178fd84e-efa1-11ea-1044-4f3a67d65ba1
md"""
Код на Си для вычисления стандартного отклонения в массиве чисел двойной точности:

```c
#include <stdlib.h>
#include <math.h>

double mean(double* vec, size_t n) {
    double sum = 0.0;
    for (size_t i = 0; i < n; i++) {
        sum += vec[i];
    }
    return sum / n;
}

double stddev(double* vec, size_t n) {
    double ave = mean(vec, n);
    double disp = 0.0;
    for (size_t i = 0; i < n; i++) {
        double diff = vec[i] - ave;
        disp += diff * diff;
    }
    return sqrt(disp / (n - 1));
}
```
"""

# ╔═╡ 0668fd20-efa1-11ea-15ed-e349ef3c3d8d
md"""
Скомпилируем этот код в виде библиотеки, которую будем вызывать из Julia
"""

# ╔═╡ 4b86d9c6-f15a-11ea-2e60-c7e7c7581d5e
C_code = raw"""
	#include <stdlib.h>
	#include <math.h>

	double mean(double* vec, size_t n) {
		double sum = 0.0;
		for (size_t i = 0; i < n; i++) {
			sum += vec[i];
		}
		return sum / n;
	}

	double stddev(double* vec, size_t n) {
		double ave = mean(vec, n);
		double disp = 0.0;
		for (size_t i = 0; i < n; i++) {
			double diff = vec[i] - ave;
			disp += diff * diff;
		}
		return sqrt(disp / (n - 1));
	}
""";

# ╔═╡ 58c2a2fa-f15a-11ea-286e-7d9efe6c15bc
# your compiled C code must have a library name
# it MUST be declared as a `const` for `ccall` to work
const Clib = tempname();

# ╔═╡ f310677c-efa0-11ea-1eb2-d7f61bb244dc
# compile the C code into a shared library
open(`gcc -fPIC -O3 -xc -shared -o $(Clib * "." * Libdl.dlext) -`, "w") do f
  print(f, C_code)
end

# ╔═╡ 9229fa1c-efa1-11ea-18a8-5f24e7c89ea5
md"""
Функцию на Си можно вызвать из Julia, доопределив "функцию-обёртку". Определим эту обёртку, чтобы можно было замерить скорость работы функции на Си из Julia.
"""

# ╔═╡ c8cd2148-efa1-11ea-34bf-9bfd87dc49ca
stddev_c(v::Vector) = ccall(
    (:stddev, Clib), # ("function_name", library)
    Float64, # return type from the C library
    (Ptr{Float64}, Csize_t), # input parameter type to the C function
    v, length(v) # actual values to pass to C function
    );

# ╔═╡ 56041bca-efa2-11ea-3a50-2324c4dd7745
md"""
Протестируем эту функцию на массиве из миллиона случайных чисел с плавающей точкой.
"""

# ╔═╡ 347f79a4-efa2-11ea-3590-75b2a0cdd22c
const v = rand(1_000_000);

# ╔═╡ 6e19fa24-efa2-11ea-10e7-f75db777fee2
md"""
Проверим, что ответ получается адекватным с теоретической точки зрения: дисперсия равномерного распределения на $[0;1]$ равна
```math
\sigma_{uniform}^2 = \int_0^1 \left(x - \frac{1}{2} \right)^2 dx = \left. \left( \frac{x^3}{3} - \frac{x^2}{2} + \frac{1}{4}\right) \right\vert_{x=0}^{1} = \frac{1}{12}
```
"""

# ╔═╡ 13c92734-efb4-11ea-1ec6-07d290119303
1 / stddev_c(v)^2

# ╔═╡ f7c8b146-efb6-11ea-35e3-57810ba8630a
md"""
Замерим время выполнения функции:
"""

# ╔═╡ 40bbd97e-efa2-11ea-1ad8-4ff37ceb24d0
@benchmark stddev_c(v)

# ╔═╡ 6371eb32-efb3-11ea-0f18-6f60dd9a6904
md"""
Поскольку функция проходит по всем элементам массива, то данные замера показывают время на обработку одного элемента около 1.5 нс. При тактовой частоте процессора 3 ГГц это означает 4-5 тактов процессора на один элемент, что примерно согласуется с числом элементарных арифметических операций в написанной функции. То есть можно сделать вывод, что функция, написанная на Си, показывает близкую к теоретически возможной производительность (на самом деле, можно несколько улучшить результат, разрешив компилятору воспользоваться векторными инструкциями процессора).

К сожалению, функция на Си работает с единственным типом данных - массивом чисел с плавающей точкой. Если в неё передать массив с неверным типом элементов, ответ будет неправильным (т.к. функция на Си просто принимает указатель и считает данные, доступные по нему, числами с плавающей точкой).
"""

# ╔═╡ d3c092c6-efb3-11ea-38ca-edffcaa12a9d
let int_v = collect(1:1_000_000)
	stddev_c(int_v)
end

# ╔═╡ 5f1aa2c6-efb9-11ea-3fb1-d53e4482f4a5
md"""
С другой стороны, можно написать программу на более высокоуровневом языке программирования, например, на Python:

```python3
from math import sqrt

def mean_py(vec):
	total = 0.0
	n = 0
	for x in vec:
		total += x
		n += 1
	return total / n

def stddev_py(vec):
    ave = mean(vec)
    disp = 0.0
	n = 0
    for x in vec:
        diff = x - ave
        disp += diff * diff
		n += 1
    return sqrt(disp / (n - 1))
```

Представленный код работает с любым объектом Python, для которого определены перечисление элементов и операции сложения и умножения с ними. В частности, функция будет единым образом работать для массивов вне зависимости от того, находятся в них целые числа или числа с плавающей точкой.

Скорость оставляет, однако, желать лучшего: на обработку массива из миллиона случайных чисел требуется около 100 мс - в сотню раз медленнее, чем было на Си. Можно несколько улучшить результат, воспользовавшись библиотекой NumPy:
```python3
import numpy

def stddev_numpy(vec):
    n = len(vec)
    ave = numpy.sum(vec) / n
    diff = vec - ave
    disp = numpy.dot(diff, diff)
    return sqrt(disp / (n-1))
```

В этом случае скорость на числовых массивах `numpy.array` оказывается близка к функции на Си, но на других типах данных (например, `range`) опять приходим к скорости "голого" интерпретатора Python. Таким образом, приобретая скорость, приходится снова жертвовать гибкостью и применять всюду массивы вместо, возможно, более удобных типов данных.
"""

# ╔═╡ d8f77488-efa1-11ea-365f-579a22100bd0
md"""
Функция для той же задачи на Julia по виду ближе к функции на Python, чем на Си: объявлений типов входных аргументов и локальных переменных не требуется, и сама функция не привязана к конкретному типу входных аргументов.
"""

# ╔═╡ 54747b72-efbd-11ea-3f9b-3f77df9b5db7
function mean_jl(vec)
	total = 0.0
	n = 0
	for x in vec
		total += x
		n += 1
	end
	return total / n
end;

# ╔═╡ fd1caab8-efa1-11ea-0d98-61f3f5d5ebf9
function stddev_jl(vec)
    ave = mean_jl(vec)
    disp = 0.0
	n = 0
    for x in vec
        diff = x - ave
        disp += diff * diff
		n += 1
    end
    return sqrt(disp / (n - 1))
end;

# ╔═╡ 9e953f48-efa2-11ea-297a-9d73a933e5cc
md"""
Убедимся, что результаты функции на Julia и функции на Си для массивов чисел с плавающей точкой идентичны:
"""

# ╔═╡ 8f0ee682-efbd-11ea-2fd1-cfab86f47c33
stddev_jl(v) == stddev_c(v)

# ╔═╡ c11f7518-efbd-11ea-2184-c3dc32d6f8f5
md"""
Функция на Julia при этом работает столь же быстро, как на Си:
"""

# ╔═╡ 452c25a4-efa2-11ea-3409-b70a09a3a2e2
@benchmark stddev_jl(v)

# ╔═╡ ea46a530-efbd-11ea-2980-edb53fb36d92
md"""
Но применимость больше не ограничивается массивами чисел с плавающей точкой.

Функция работает с массивом целых чисел:
"""

# ╔═╡ 29a82cee-efbe-11ea-2ae5-91b8871ab2ae
stddev_jl(collect(1:1_000_000))

# ╔═╡ 52ef68b2-efbe-11ea-1942-63281d90391f
md"""
Или с объектом типа "диапазон" (хранящем арифметическую прогрессию в компактном виде):
"""

# ╔═╡ 04ff3766-efbe-11ea-1728-9f05bcdcf9bb
stddev_jl(1:1_000_000)

# ╔═╡ 9c820e88-efbe-11ea-3eca-1b49dbe7ba36
md"""
Поскольку под каждый конкретный тип входных аргументов компилируется специализированный машинный код, высокая производительность не ограничена каким-то специализированным типом данных.
"""

# ╔═╡ f639ca74-efbe-11ea-25a4-91570c2e6d8d
@benchmark stddev_jl(1:1_000_000)

# ╔═╡ Cell order:
# ╟─f3123a28-efaf-11ea-2064-4d1e5dfa338c
# ╠═c328d2a4-efa0-11ea-25d1-8b0ec2850200
# ╟─178fd84e-efa1-11ea-1044-4f3a67d65ba1
# ╟─0668fd20-efa1-11ea-15ed-e349ef3c3d8d
# ╠═4b86d9c6-f15a-11ea-2e60-c7e7c7581d5e
# ╠═58c2a2fa-f15a-11ea-286e-7d9efe6c15bc
# ╠═f310677c-efa0-11ea-1eb2-d7f61bb244dc
# ╟─9229fa1c-efa1-11ea-18a8-5f24e7c89ea5
# ╠═c8cd2148-efa1-11ea-34bf-9bfd87dc49ca
# ╟─56041bca-efa2-11ea-3a50-2324c4dd7745
# ╠═347f79a4-efa2-11ea-3590-75b2a0cdd22c
# ╟─6e19fa24-efa2-11ea-10e7-f75db777fee2
# ╠═13c92734-efb4-11ea-1ec6-07d290119303
# ╟─f7c8b146-efb6-11ea-35e3-57810ba8630a
# ╠═40bbd97e-efa2-11ea-1ad8-4ff37ceb24d0
# ╟─6371eb32-efb3-11ea-0f18-6f60dd9a6904
# ╠═d3c092c6-efb3-11ea-38ca-edffcaa12a9d
# ╟─5f1aa2c6-efb9-11ea-3fb1-d53e4482f4a5
# ╟─d8f77488-efa1-11ea-365f-579a22100bd0
# ╠═54747b72-efbd-11ea-3f9b-3f77df9b5db7
# ╠═fd1caab8-efa1-11ea-0d98-61f3f5d5ebf9
# ╟─9e953f48-efa2-11ea-297a-9d73a933e5cc
# ╠═8f0ee682-efbd-11ea-2fd1-cfab86f47c33
# ╟─c11f7518-efbd-11ea-2184-c3dc32d6f8f5
# ╠═452c25a4-efa2-11ea-3409-b70a09a3a2e2
# ╟─ea46a530-efbd-11ea-2980-edb53fb36d92
# ╠═29a82cee-efbe-11ea-2ae5-91b8871ab2ae
# ╟─52ef68b2-efbe-11ea-1942-63281d90391f
# ╠═04ff3766-efbe-11ea-1728-9f05bcdcf9bb
# ╟─9c820e88-efbe-11ea-3eca-1b49dbe7ba36
# ╠═f639ca74-efbe-11ea-25a4-91570c2e6d8d
